# -*- coding: utf-8 -*-

import mindspore
import mindspore.context as context
import x2ms_adapter
from x2ms_adapter.context import x2ms_context
from x2ms_adapter.optimizers import optim_register
from x2ms_adapter.exception import TrainBreakException, TrainContinueException, TrainReturnException
from x2ms_adapter.optimizers import optim_register
import mindspore
import x2ms_adapter
import x2ms_adapter.datasets as x2ms_datasets
import x2ms_adapter.loss as loss_wrapper
import x2ms_adapter.nn_cell
import x2ms_adapter.nn_init
import x2ms_adapter.numpy as x2ms_np

if not x2ms_context.is_context_init:
    context.set_context(mode=context.PYNATIVE_MODE, pynative_synchronize=True)
    x2ms_context.is_context_init = True
import logging
import argparse
import math
import os
import sys
import random
import numpy

from sklearn import metrics
from time import strftime, localtime

from pytorch_pretrained_bert import BertModel

from data_utils_bert import build_tokenizer, build_embedding_matrix, Tokenizer4Bert, ABSADataset
from models import AAGCN_BERT

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))


class Instructor:
    def __init__(self, opt):
        self.opt = opt

        if 'bert' in opt.model_name:
            tokenizer = Tokenizer4Bert(opt.max_seq_len, opt.pretrained_bert_name)
            bert = BertModel.from_pretrained(opt.pretrained_bert_name)
            self.model = x2ms_adapter.to(opt.model_class(bert, opt), opt.device)
        else:
            tokenizer = build_tokenizer(
                fnames=[opt.dataset_file['train'], opt.dataset_file['test']],
                max_seq_len=opt.max_seq_len,
                dat_fname='{0}_tokenizer.dat'.format(opt.dataset))
            embedding_matrix = build_embedding_matrix(
                word2idx=tokenizer.word2idx,
                embed_dim=opt.embed_dim,
                dat_fname='{0}_{1}_embedding_matrix.dat'.format(str(opt.embed_dim), opt.dataset))
            self.model = x2ms_adapter.to(opt.model_class(embedding_matrix, opt), opt.device)

        self.trainset = ABSADataset(opt.dataset_file['train'], tokenizer)
        self.testset = ABSADataset(opt.dataset_file['test'], tokenizer)
        assert 0 <= opt.valset_ratio < 1
        if opt.valset_ratio > 0:
            valset_len = int(len(self.trainset) * opt.valset_ratio)
            self.trainset, self.valset = x2ms_datasets.random_split(self.trainset, (len(self.trainset)-valset_len, valset_len))
        else:
            self.valset = self.testset

        if opt.device.type == 'cuda':
            logger.info('cuda memory allocated: {}'.format(x2ms_adapter.memory_allocated(device=opt.device.index)))
        self._print_args()

    def _print_args(self):
        n_trainable_params, n_nontrainable_params = 0, 0
        for p in x2ms_adapter.parameters(self.model):
            n_params = x2ms_adapter.prod(x2ms_adapter.x2ms_tensor(p.shape))
            if p.requires_grad:
                n_trainable_params += n_params
            else:
                n_nontrainable_params += n_params
        logger.info('> n_trainable_params: {0}, n_nontrainable_params: {1}'.format(n_trainable_params, n_nontrainable_params))
        logger.info('> training arguments:')
        for arg in vars(self.opt):
            logger.info('>>> {0}: {1}'.format(arg, getattr(self.opt, arg)))

    def _reset_params(self):
        for child in x2ms_adapter.nn_cell.children(self.model):
            if type(child) != BertModel:
                for p in x2ms_adapter.parameters(child):
                    if p.requires_grad:
                        if len(p.shape) > 1:
                            self.opt.initializer(p)
                        else:
                            stdv = 1. / x2ms_adapter.tensor_api.sqrt(math, p.shape[0])
                            x2ms_adapter.nn_init.uniform_(p, a=-stdv, b=stdv)

    def _train(self, criterion, optimizer, train_data_loader, val_data_loader):
        max_val_acc = 0
        max_val_f1 = 0
        max_val_epoch = 0
        global_step = 0
        path = None
        for i_epoch in range(self.opt.num_epoch):
            logger.info('>' * 100)
            logger.info('epoch: {}'.format(i_epoch))
            n_correct, n_total, loss_total = 0, 0, 0
            x2ms_adapter.x2ms_train(self.model)
            class WithLossCell(mindspore.nn.Cell):
                def __init__(self, train_obj=None):
                    super(WithLossCell, self).__init__(auto_prefix=False)
                    self._input = None
                    self._output = None
                    self.train_obj = train_obj
                    self.amp_model = x2ms_context.amp_model

                def construct(self):
                    nonlocal global_step
                    
                    i_batch, batch = self._input
                    global_step += 1
                    x2ms_adapter.nn_cell.zero_grad(optimizer)

                    inputs = [x2ms_adapter.to(batch[col], self.train_obj.opt.device) for col in self.train_obj.opt.inputs_cols]
                    outputs = self.train_obj.model(inputs)
                    targets = x2ms_adapter.to(batch['polarity'], self.train_obj.opt.device)

                    loss = criterion(outputs, targets)
                    self._output = (inputs, loss, outputs, targets)
                    return loss

                @property
                def output(self):
                    return self._output
            wrapped_model = WithLossCell(self)
            wrapped_model = x2ms_adapter.train_one_step_cell(wrapped_model, optim_register.get_instance())
            for i_batch, batch in enumerate(train_data_loader):
                try:
                    wrapped_model.network._input = (i_batch, batch)
                    wrapped_model()
                except TrainBreakException:
                    break
                except TrainContinueException:
                    continue
                except TrainReturnException:
                    return
                    
                inputs, loss, outputs, targets = wrapped_model.network.output
                optimizer.step()

                n_correct += x2ms_adapter.tensor_api.item(x2ms_adapter.tensor_api.x2ms_sum((x2ms_adapter.argmax(outputs, -1) == targets)))
                n_total += len(outputs)
                loss_total += x2ms_adapter.tensor_api.item(loss) * len(outputs)
                if global_step % self.opt.log_step == 0:
                    train_acc = n_correct / n_total
                    train_loss = loss_total / n_total
                    logger.info('loss: {:.4f}, acc: {:.4f}'.format(train_loss, train_acc))

                val_acc, val_f1 = self._evaluate_acc_f1(val_data_loader)
                logger.info('> val_acc: {:.4f}, val_f1: {:.4f}'.format(val_acc, val_f1))
                if val_acc > max_val_acc:
                    max_val_acc = val_acc
                    max_val_epoch = i_epoch
                    if not os.path.exists('state_dict'):
                        os.mkdir('state_dict')
                    path = 'state_dict/{0}_{1}_val_acc_{2}'.format(self.opt.model_name, self.opt.dataset, round(val_acc, 4))
                    x2ms_adapter.save(x2ms_adapter.state_dict(self.model), path)
                    logger.info('>> saved: {}'.format(path))
                if val_f1 > max_val_f1:
                    max_val_f1 = val_f1
            if i_epoch - max_val_epoch >= self.opt.patience:
                print('>> early stop.')
                break

        return path

    def _evaluate_acc_f1(self, data_loader):
        n_correct, n_total = 0, 0
        t_targets_all, t_outputs_all = None, None
        x2ms_adapter.x2ms_eval(self.model)
        for i_batch, t_batch in enumerate(data_loader):
            t_inputs = [x2ms_adapter.to(t_batch[col], self.opt.device) for col in self.opt.inputs_cols]
            t_targets = x2ms_adapter.to(t_batch['polarity'], self.opt.device)
            t_outputs = self.model(t_inputs)

            n_correct += x2ms_adapter.tensor_api.item(x2ms_adapter.tensor_api.x2ms_sum((x2ms_adapter.argmax(t_outputs, -1) == t_targets)))
            n_total += len(t_outputs)

            if t_targets_all is None:
                t_targets_all = t_targets
                t_outputs_all = t_outputs
            else:
                t_targets_all = x2ms_adapter.cat((t_targets_all, t_targets), dim=0)
                t_outputs_all = x2ms_adapter.cat((t_outputs_all, t_outputs), dim=0)

        acc = n_correct / n_total
        f1 = x2ms_np.sklearn_metrics_f1_score(t_targets_all, x2ms_adapter.argmax(t_outputs_all, -1), labels=[0, 1, 2], average='macro')
        return acc, f1

    def run(self):
        criterion = loss_wrapper.CrossEntropyLoss()
        _params = filter(lambda p: p.requires_grad, x2ms_adapter.parameters(self.model))
        optimizer = self.opt.optimizer(_params, lr=self.opt.lr, weight_decay=self.opt.l2reg)

        train_data_loader = x2ms_datasets.DataLoader(dataset=self.trainset, batch_size=self.opt.batch_size, shuffle=True)
        test_data_loader = x2ms_datasets.DataLoader(dataset=self.testset, batch_size=self.opt.batch_size, shuffle=False)
        val_data_loader = x2ms_datasets.DataLoader(dataset=self.valset, batch_size=self.opt.batch_size, shuffle=False)

        self._reset_params()
        best_model_path = self._train(criterion, optimizer, train_data_loader, val_data_loader)
        x2ms_adapter.load_state_dict(self.model, x2ms_adapter.load(best_model_path))
        test_acc, test_f1 = self._evaluate_acc_f1(test_data_loader)
        logger.info('>> test_acc: {:.4f}, test_f1: {:.4f}'.format(test_acc, test_f1))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_name', default='aagcn_bert', type=str)
    parser.add_argument('--dataset', default='rest15_sen', type=str)
    parser.add_argument('--optimizer', default='adam', type=str)
    parser.add_argument('--initializer', default='xavier_uniform_', type=str)
    parser.add_argument('--lr', default=2e-5, type=float, help='try 5e-5, 2e-5 for BERT, 1e-3 for others')
    parser.add_argument('--dropout', default=0.1, type=float)
    parser.add_argument('--l2reg', default=0.01, type=float)
    parser.add_argument('--num_epoch', default=20, type=int, help='try larger number for non-BERT models')
    parser.add_argument('--batch_size', default=16, type=int, help='try 16, 32, 64 for BERT models')
    parser.add_argument('--log_step', default=10, type=int)
    parser.add_argument('--embed_dim', default=300, type=int)
    parser.add_argument('--hidden_dim', default=300, type=int)
    parser.add_argument('--bert_dim', default=768, type=int)
    parser.add_argument('--pretrained_bert_name', default='bert-base-uncased', type=str)
    parser.add_argument('--max_seq_len', default=85, type=int)
    parser.add_argument('--polarities_dim', default=3, type=int)
    parser.add_argument('--hops', default=3, type=int)
    parser.add_argument('--patience', default=5, type=int)
    parser.add_argument('--device', default=None, type=str, help='e.g. cuda:0')
    parser.add_argument('--seed', default=1234, type=int, help='set seed for reproducibility')
    parser.add_argument('--valset_ratio', default=0.1, type=float, help='set ratio between 0 and 1 for validation support')
    parser.add_argument('--local_context_focus', default='cdm', type=str, help='local context focus mode, cdw or cdm')
    parser.add_argument('--SRD', default=3, type=int, help='semantic-relative-distance, see the paper of LCF-BERT model')
    opt = parser.parse_args()


    model_classes = {
        'aagcn_bert': AAGCN_BERT,
    }
    dataset_files = {
        'rest15_sen': {
            'train': './dataset/senticnet/15_rest_train.raw.tokenized',
            'test': './dataset/senticnet/15_rest_test.raw.tokenized'
        },
        'lap15_sen': {
            'train': './dataset/senticnet/15_lap_train.raw.tokenized',
            'test': './dataset/senticnet/15_lap_test.raw.tokenized'
        },
        'rest15_con': {
            'train': './dataset/conceptnet/15_rest_train.raw.tokenized',
            'test': './dataset/conceptnet/15_rest_test.raw.tokenized'
        },
        'lap15_con': {
            'train': './dataset/conceptnet/15_lap_train.raw.tokenized',
            'test': './dataset/conceptnet/15_lap_test.raw.tokenized'
        },

        'rest16_sen': {
            'train': './dataset/senticnet/16_rest_train.raw.tokenized',
            'test': './dataset/senticnet/16_rest_test.raw.tokenized'
        },
        'lap16_sen': {
            'train': './dataset/senticnet/16_lap_train.raw.tokenized',
            'test': './dataset/senticnet/16_lap_test.raw.tokenized'
        },
        'rest16_con': {
            'train': './dataset/conceptnet/16_rest_train.raw.tokenized',
            'test': './dataset/conceptnet/16_rest_test.raw.tokenized'
        },
        'lap16_con': {
            'train': './dataset/conceptnet/16_lap_train.raw.tokenized',
            'test': './dataset/conceptnet/16_lap_test.raw.tokenized'
        },
        '14_con': {
            'train': './dataset/conceptnet/14_train.raw.tokenized',
            'test': './dataset/conceptnet/14_test.raw.tokenized'
        },
        '14_sen': {
            'train': './dataset/senticnet/14_train.raw.tokenized',
            'test': './dataset/senticnet/14_test.raw.tokenized'
        }
    }
    input_colses = {
        'aagcn_bert': ['concat_bert_indices', 'concat_segments_indices', 'entity_graph', 'attribute_graph'],

    }
    initializers = {
        'xavier_uniform_': x2ms_adapter.nn_init.xavier_uniform_,
        'xavier_normal_': x2ms_adapter.nn_init.xavier_normal_,
        'orthogonal_': x2ms_adapter.nn_init.orthogonal_,
    }
    optimizers = {
        'adadelta': optim_register.adadelta,
        'adagrad': optim_register.adagrad,
        'adam': optim_register.adam,
        'adamax': optim_register.adamax,
        'asgd': optim_register.asgd,
        'rmsprop': optim_register.rmsprop,
        'sgd': optim_register.sgd,
    }
    opt.model_class = model_classes[opt.model_name]
    opt.dataset_file = dataset_files[opt.dataset]
    opt.inputs_cols = input_colses[opt.model_name]
    opt.initializer = initializers[opt.initializer]
    opt.optimizer = optimizers[opt.optimizer]
    opt.device = x2ms_adapter.Device('cuda' if x2ms_adapter.is_cuda_available() else 'cpu') \
        if opt.device is None else x2ms_adapter.Device(opt.device)

    log_file = '{}-{}-{}.log'.format(opt.model_name, opt.dataset, strftime("%y%m%d-%H%M", localtime()))
    logger.addHandler(logging.FileHandler(log_file))

    if opt.seed is not None:
        print('\n=======================> seed:', opt.seed, '\n')
        random.seed(opt.seed)
        numpy.random.seed(opt.seed)
        mindspore.set_seed(opt.seed)
        mindspore.set_seed(opt.seed)
    os.environ['PYTHONHASHSEED'] = str(opt.seed)

    ins = Instructor(opt)
    ins.run()


if __name__ == '__main__':
    main()
