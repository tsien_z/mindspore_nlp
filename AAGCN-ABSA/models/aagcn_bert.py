# -*- coding: utf-8 -*-

import math
from layers.dynamic_rnn import DynamicLSTM
import mindspore
import mindspore.nn
import x2ms_adapter
import x2ms_adapter.nn as nn
import x2ms_adapter.nn_functional

class GraphConvolution(mindspore.nn.Cell):
    def __init__(self, in_features, out_features, bias=True):
        super(GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = mindspore.Parameter(x2ms_adapter.FloatTensor(in_features, out_features))
        if bias:
            self.bias = mindspore.Parameter(x2ms_adapter.FloatTensor(out_features))
        else:
            self.register_parameter('bias', None)

    def construct(self, text, adj):
        hidden = x2ms_adapter.matmul(text, self.weight)
        denom = x2ms_adapter.x2ms_sum(adj, dim=2, keepdim=True) + 1
        output = x2ms_adapter.matmul(adj, x2ms_adapter.tensor_api.x2ms_float(hidden)) / denom
        if self.bias is not None:
            return output + self.bias
        else:
            return output

class AAGCN_BERT(mindspore.nn.Cell):
    def __init__(self, bert, opt):
        super(AAGCN_BERT, self).__init__()
        self.opt = opt
        self.bert = bert
        self.gc1 = GraphConvolution(opt.bert_dim, opt.bert_dim)
        self.gc2 = GraphConvolution(opt.bert_dim, opt.bert_dim)
        self.gc3 = GraphConvolution(opt.bert_dim, opt.bert_dim)
        self.gc4 = GraphConvolution(opt.bert_dim, opt.bert_dim)
        self.gc5 = GraphConvolution(opt.bert_dim, opt.bert_dim)
        self.gc6 = GraphConvolution(opt.bert_dim, opt.bert_dim)
        self.gc7 = GraphConvolution(opt.bert_dim, opt.bert_dim)
        self.gc8 = GraphConvolution(opt.bert_dim, opt.bert_dim)


        self.fc = nn.Linear(opt.bert_dim, opt.polarities_dim)
        self.fc2 = nn.Linear(4 * opt.hidden_dim, opt.polarities_dim)
        self.dfc = nn.Linear(4*opt.hidden_dim, opt.polarities_dim)
        self.text_embed_dropout = nn.Dropout(0.3)

    def construct(self, inputs):

        text_bert_indices, bert_segments_ids, e_adj, a_adj = inputs
        encoder_layer, pooled_output = self.bert(text_bert_indices, token_type_ids=bert_segments_ids, output_all_encoded_layers=False)
        text_out = encoder_layer
        x = x2ms_adapter.nn_functional.relu(self.gc1(text_out, e_adj))
        x = x2ms_adapter.nn_functional.relu(self.gc2(x, a_adj))

        x = x2ms_adapter.nn_functional.relu(self.gc3(x, e_adj))
        x = x2ms_adapter.nn_functional.relu(self.gc4(x, a_adj))

        alpha_mat = x2ms_adapter.matmul(x, x2ms_adapter.tensor_api.transpose(text_out, 1, 2))
        alpha = x2ms_adapter.nn_functional.softmax(x2ms_adapter.tensor_api.x2ms_sum(alpha_mat, 1, keepdim=True), dim=2)
        x = x2ms_adapter.tensor_api.squeeze(x2ms_adapter.matmul(alpha, text_out), 1)

        output = self.fc(x)
        return output
