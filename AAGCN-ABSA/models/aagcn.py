# -*- coding: utf-8 -*-
from layers.dynamic_rnn import DynamicLSTM
import numpy as np
import mindspore
import mindspore.nn as nn
from mindspore import Tensor, ops
from mindspore.common.initializer import Normal

# mindspore的Embedding类暂不支持from_pretrained类方法，这里自己实现了一个Embedding类包含类方法from_pretrained_embedding
class Embedding(nn.Embedding):
    def __init__(self, vocab_size, embedding_size, use_one_hot=False, embedding_table='normal', dtype=mindspore.float32,
                 padding_idx=None):
        if embedding_table == 'normal':
            embedding_table = Normal(1.0)
        super().__init__(vocab_size, embedding_size, use_one_hot, embedding_table, dtype, padding_idx)

    @classmethod
    def from_pretrained_embedding(cls, embeddings:Tensor, freeze=True, padding_idx=None):
        rows, cols = embeddings.shape
        embedding = cls(rows, cols, embedding_table=embeddings, padding_idx=padding_idx)
        embedding.embedding_table.requires_grad = not freeze

        return embedding

class GraphConvolution(nn.Cell):
    def __init__(self, in_features, out_features, bias=True):
        super(GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        # ms的Parameter需要传入有初值的tensor，这里使用np.ones构造
        self.weight = mindspore.Parameter(Tensor(np.ones((in_features, out_features)), mindspore.float32))
        if bias:
            self.bias = mindspore.Parameter(Tensor(np.ones((in_features)), mindspore.float32))
        else:
            self.bias = None
        self.type_dst = mindspore.int32
        self.cast = ops.Cast()

    def construct(self, text, adj):
        # torch.matmul对应ops.matmul
        hidden = ops.matmul(text, self.weight)
        # 不支持torch.Tensor的.float()写法，需要调用cast转换
        hidden_fp32 = self.cast(hidden, self.type_dst)
        # 使用ms的Tensor.sum() 传参为axis和keepdims 和torch不用
        denom = adj.sum(axis=2, keepdims=True) + 1
        output = ops.matmul(adj, hidden_fp32) / denom
        if self.bias is not None:
            return output + self.bias
        else:
            return output

# 实现网络模型类AAGCN继承自Cell 类似于torch中的继承Module
class AAGCN(nn.Cell):
    def __init__(self, embedding_matrix, opt):
        super(AAGCN, self).__init__()
        self.opt = opt
        # 这里的Embedding类是自定义改写的
        self.embed = Embedding.from_pretrained_embedding(Tensor(embedding_matrix, dtype=mindspore.float16))
        self.text_lstm = DynamicLSTM(opt.embed_dim, opt.hidden_dim, num_layers=1, batch_first=True, bidirectional=True)
        self.gc1 = GraphConvolution(2*opt.hidden_dim, 2*opt.hidden_dim)
        self.gc2 = GraphConvolution(2*opt.hidden_dim, 2*opt.hidden_dim)
        self.gc3 = GraphConvolution(2*opt.hidden_dim, 2*opt.hidden_dim)
        self.gc4 = GraphConvolution(2*opt.hidden_dim, 2*opt.hidden_dim)
        # self.gc5 = GraphConvolution(2*opt.hidden_dim, 2*opt.hidden_dim)
        # self.gc6 = GraphConvolution(2*opt.hidden_dim, 2*opt.hidden_dim)

        # mindspore.nn.Dense等价torch.nn.Linear
        self.fc  = nn.Dense(2*opt.hidden_dim, opt.polarities_dim)
        # self.fc2 = nn.Dense(4*opt.hidden_dim, opt.polarities_dim)
        # self.dfc = nn.Dense(4*opt.hidden_dim, opt.polarities_dim)
        # Dropout也可以等价替换
        self.text_embed_dropout = nn.Dropout(0.3)
        self.relu = nn.ReLU()
        self.transpose = ops.Transpose()
        # Softmax和Squeeze在类实例化时就要指定aixs
        self.softmax = ops.Softmax(axis=2)
        self.squeeze = ops.Squeeze(axis=1)
        self.cast = ops.Cast()

    def construct(self, inputs):
        text_indices, adj, d_adj = inputs[0], inputs[2], inputs[3]
        text_no_empty = text_indices != 0
        text_len = self.cast(text_no_empty.sum(axis=-1), mindspore.float32)
        text = self.embed(text_indices)
        text = self.text_embed_dropout(text)
        text_out, (_, _) = self.text_lstm(text, text_len)
        x = self.relu(self.gc1(text_out, adj))
        x = self.relu(self.gc2(x, d_adj))

        x = self.relu(self.gc3(x, adj))
        x = self.relu(self.gc4(x, d_adj))

        alpha_mat =  ops.matmul(x, self.transpose(text_out, (0, 2, 1)))
        alpha = self.softmax(alpha_mat.sum(axis=1, keepdims=True))
        x = self.squeeze(ops.matmul(alpha, text_out))

        output = self.fc(x)
        return output
