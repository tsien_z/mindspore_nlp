# -*- coding: utf-8 -*-

import math
import ipdb
import mindspore
import mindspore.nn
import x2ms_adapter
import x2ms_adapter.nn as nn
import x2ms_adapter.nn_functional

class Attention(mindspore.nn.Cell):
    def __init__(self, embed_dim, hidden_dim=None, out_dim=None, n_head=1, score_function='dot_product', dropout=0):
        super(Attention, self).__init__()
        if hidden_dim is None:
            hidden_dim = embed_dim // n_head
        if out_dim is None:
            out_dim = embed_dim
        self.embed_dim = embed_dim
        self.hidden_dim = hidden_dim
        self.n_head = n_head
        self.score_function = score_function
        self.w_k = nn.Linear(embed_dim, n_head * hidden_dim)
        self.w_q = nn.Linear(embed_dim, n_head * hidden_dim)
        self.proj = nn.Linear(n_head * hidden_dim, out_dim)
        self.dropout = nn.Dropout(dropout)
        if score_function == 'mlp':
            self.weight = mindspore.Parameter(x2ms_adapter.Tensor(hidden_dim*2))
        elif self.score_function == 'bi_linear':
            self.weight = mindspore.Parameter(x2ms_adapter.Tensor(hidden_dim, hidden_dim))
        else:
            self.register_parameter('weight', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / x2ms_adapter.tensor_api.sqrt(math, self.hidden_dim)
        if self.weight is not None:
            x2ms_adapter.tensor_api.uniform_(self.weight.data, -stdv, stdv)

    def construct(self, k, q):
        if len(q.shape) == 2:
            q = x2ms_adapter.unsqueeze(q, dim=1)
        if len(k.shape) == 2:
            k = x2ms_adapter.unsqueeze(k, dim=1)
        mb_size = k.shape[0]
        k_len = k.shape[1]
        q_len = q.shape[1]
        kx = x2ms_adapter.tensor_api.view(self.w_k(k), mb_size, k_len, self.n_head, self.hidden_dim)
        kx = x2ms_adapter.tensor_api.view(x2ms_adapter.tensor_api.contiguous(x2ms_adapter.tensor_api.permute(kx, 2, 0, 1, 3)), -1, k_len, self.hidden_dim)
        qx = x2ms_adapter.tensor_api.view(self.w_q(q), mb_size, q_len, self.n_head, self.hidden_dim)
        qx = x2ms_adapter.tensor_api.view(x2ms_adapter.tensor_api.contiguous(x2ms_adapter.tensor_api.permute(qx, 2, 0, 1, 3)), -1, q_len, self.hidden_dim)
        if self.score_function == 'dot_product':
            kt = x2ms_adapter.tensor_api.permute(kx, 0, 2, 1)
            score = x2ms_adapter.bmm(qx, kt)
        elif self.score_function == 'scaled_dot_product':
            kt = x2ms_adapter.tensor_api.permute(kx, 0, 2, 1)
            qkt = x2ms_adapter.bmm(qx, kt)
            score = x2ms_adapter.div(qkt, x2ms_adapter.tensor_api.sqrt(math, self.hidden_dim))
        elif self.score_function == 'mlp':
            kxx = x2ms_adapter.tensor_api.expand(x2ms_adapter.unsqueeze(kx, dim=1), -1, q_len, -1, -1)
            qxx = x2ms_adapter.tensor_api.expand(x2ms_adapter.unsqueeze(qx, dim=2), -1, -1, k_len, -1)
            kq = x2ms_adapter.cat((kxx, qxx), dim=-1)
            score = x2ms_adapter.nn_functional.tanh(x2ms_adapter.matmul(kq, self.weight))
        elif self.score_function == 'bi_linear':
            qw = x2ms_adapter.matmul(qx, self.weight)
            kt = x2ms_adapter.tensor_api.permute(kx, 0, 2, 1)
            score = x2ms_adapter.bmm(qw, kt)
        else:
            raise RuntimeError('invalid score_function')
        score = x2ms_adapter.nn_functional.softmax(score, dim=-1)
        output = x2ms_adapter.bmm(score, kx)
        output = x2ms_adapter.cat(x2ms_adapter.split(output, mb_size, dim=0), dim=-1)
        output = self.proj(output)
        output = self.dropout(output)
        return output, score


class Attention_Masked(mindspore.nn.Cell):
    def __init__(self, embed_dim, hidden_dim=None, out_dim=None, n_head=1, score_function='dot_product', dropout=0):
        super(Attention_Masked, self).__init__()
        if hidden_dim is None:
            hidden_dim = embed_dim // n_head
        if out_dim is None:
            out_dim = embed_dim
        self.embed_dim = embed_dim
        self.hidden_dim = hidden_dim
        self.n_head = n_head
        self.score_function = score_function
        self.w_k = nn.Linear(embed_dim, n_head * hidden_dim)
        self.w_q = nn.Linear(embed_dim, n_head * hidden_dim)
        self.proj = nn.Linear(n_head * hidden_dim, out_dim)
        self.dropout = nn.Dropout(dropout)
        if score_function == 'mlp':
            self.weight = mindspore.Parameter(x2ms_adapter.Tensor(hidden_dim*2))
        elif self.score_function == 'bi_linear':
            self.weight = mindspore.Parameter(x2ms_adapter.Tensor(hidden_dim, hidden_dim))
        else:
            self.register_parameter('weight', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / x2ms_adapter.tensor_api.sqrt(math, self.hidden_dim)
        if self.weight is not None:
            x2ms_adapter.tensor_api.uniform_(self.weight.data, -stdv, stdv)

    def construct(self, k, q,k_step,q_step):
        
        if len(q.shape) == 2:
            q = x2ms_adapter.unsqueeze(q, dim=1)
        if len(k.shape) == 2:
            k = x2ms_adapter.unsqueeze(k, dim=1)
        mb_size = k.shape[0]
        k_len = k.shape[1]
        q_len = q.shape[1]
        ipdb.set_trace()
        mask = x2ms_adapter.tensor_api.unsqueeze(x2ms_adapter.x2ms_tensor([[0]*step + [-10000]*(k_len-step) for step in x2ms_adapter.tensor_api.numpy(k_step)],dtype=mindspore.float32 ), 1)
        mask = x2ms_adapter.cat([mask for _ in range(self.n_head)],dim=0)

        kx = x2ms_adapter.tensor_api.view(self.w_k(k), mb_size, k_len, self.n_head, self.hidden_dim)
        kx = x2ms_adapter.tensor_api.view(x2ms_adapter.tensor_api.contiguous(x2ms_adapter.tensor_api.permute(kx, 2, 0, 1, 3)), -1, k_len, self.hidden_dim)
        qx = x2ms_adapter.tensor_api.view(self.w_q(q), mb_size, q_len, self.n_head, self.hidden_dim)
        qx = x2ms_adapter.tensor_api.view(x2ms_adapter.tensor_api.contiguous(x2ms_adapter.tensor_api.permute(qx, 2, 0, 1, 3)), -1, q_len, self.hidden_dim)
        if self.score_function == 'dot_product':
            kt = x2ms_adapter.tensor_api.permute(kx, 0, 2, 1)
            score = x2ms_adapter.bmm(qx, kt)
        elif self.score_function == 'scaled_dot_product':
            kt = x2ms_adapter.tensor_api.permute(kx, 0, 2, 1)
            qkt = x2ms_adapter.bmm(qx, kt)
            score = x2ms_adapter.div(qkt, x2ms_adapter.tensor_api.sqrt(math, self.hidden_dim))
        elif self.score_function == 'mlp':
            kxx = x2ms_adapter.tensor_api.expand(x2ms_adapter.unsqueeze(kx, dim=1), -1, q_len, -1, -1)
            qxx = x2ms_adapter.tensor_api.expand(x2ms_adapter.unsqueeze(qx, dim=2), -1, -1, k_len, -1)
            kq = x2ms_adapter.cat((kxx, qxx), dim=-1)
            score = x2ms_adapter.nn_functional.tanh(x2ms_adapter.matmul(kq, self.weight))
        elif self.score_function == 'bi_linear':
            qw = x2ms_adapter.matmul(qx, self.weight)
            kt = x2ms_adapter.tensor_api.permute(kx, 0, 2, 1)
            score = x2ms_adapter.bmm(qw, kt)
        else:
            raise RuntimeError('invalid score_function')
        score = score + mask
        score = x2ms_adapter.nn_functional.softmax(score, dim=-1)
        output = x2ms_adapter.bmm(score, kx)
        output = x2ms_adapter.cat(x2ms_adapter.split(output, mb_size, dim=0), dim=-1) 
        output = self.proj(output) 
        output = self.dropout(output)
        return output, score




class NoQueryAttention(Attention):
    def __init__(self, embed_dim, hidden_dim=None, out_dim=None, n_head=1, score_function='dot_product', q_len=1, dropout=0):
        super(NoQueryAttention, self).__init__(embed_dim, hidden_dim, out_dim, n_head, score_function, dropout)
        self.q_len = q_len
        self.q = mindspore.Parameter(x2ms_adapter.Tensor(q_len, embed_dim))
        self.reset_q()

    def reset_q(self):
        stdv = 1. / x2ms_adapter.tensor_api.sqrt(math, self.embed_dim)
        x2ms_adapter.tensor_api.uniform_(self.q.data, -stdv, stdv)

    def construct(self, k, **kwargs):
        mb_size = k.shape[0]
        q = x2ms_adapter.tensor_api.expand(self.q, mb_size, -1, -1)
        return x2ms_adapter.forward(super(NoQueryAttention, self), k, q)
