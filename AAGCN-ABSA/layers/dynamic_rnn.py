# -*- coding: utf-8 -*-


import numpy as np
import mindspore as ms
import mindspore.nn as nn
from mindspore import ops

# 实现AAGCN中的DynamicLSTM层
class DynamicLSTM(nn.Cell):
    def __init__(self, input_size, hidden_size, num_layers=1, bias=True, batch_first=True, dropout=0.,
                 bidirectional=False, only_use_last_hidden_state=False, rnn_type = 'LSTM'):
        super(DynamicLSTM, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.bias = bias
        self.batch_first = batch_first
        self.dropout = dropout
        self.bidirectional = bidirectional
        self.only_use_last_hidden_state = only_use_last_hidden_state
        self.rnn_type = rnn_type
        # 实例化了Sort计算，construct前向计算需要使用
        self.sort = ops.Sort()
        # 实例化了Transpose计算，construct前向计算需要使用
        self.transpose = ops.Transpose()
        # 例化了Cast计算，construct前向计算需要使用
        self.cast = ops.Cast()

        if self.rnn_type == 'LSTM': 
            # torch.nn.LSTM与mindspore.nn.LSTM功能基本相同，除了参数名has_bias
            self.RNN = nn.LSTM(
                input_size=input_size, hidden_size=hidden_size, num_layers=num_layers,
                has_bias=bias, batch_first=batch_first, dropout=dropout, bidirectional=bidirectional)  
        elif self.rnn_type == 'GRU':
            # torch.nn.GRU与mindspore.nn.GRU功能基本相同，除了参数名has_bias
            self.RNN = nn.GRU(
                input_size=input_size, hidden_size=hidden_size, num_layers=num_layers,
                has_bias=bias, batch_first=batch_first, dropout=dropout, bidirectional=bidirectional)
        elif self.rnn_type == 'RNN':
            # torch.nn.RNN与mindspore.nn.RNN功能基本相同，除了参数名has_bias
            self.RNN = nn.RNN(
                input_size=input_size, hidden_size=hidden_size, num_layers=num_layers,
                has_bias=bias, batch_first=batch_first, dropout=dropout, bidirectional=bidirectional)


    def construct(self, x, x_len, h0=None):
        # ms的sort返回为排序后的原数据和数据索引，这里只需要索引，所以需要加 x_sort_idx = x_sort[1]
        x_sort = self.sort(-x_len)
        x_sort_idx = x_sort[1]
        # ms的sort只支持输入数据为fp16或fp32数据类型，所以这里需要显式的加cast
        x_unsort = self.sort(self.cast(x_sort_idx, ms.float32))
        x_unsort_idx = x_unsort[1]
        x_len = x_len[x_sort_idx]
        x = x[x_sort_idx]
        if self.rnn_type == 'LSTM':
            if h0 is None: 
                out, (ht, ct) = self.RNN(x, None)
            else:
                out, (ht, ct) = self.RNN(x, (h0, h0))
        else: 
            if h0 is None:
                out, ht = self.RNN(x, None)
            else:
                out, ht = self.RNN(x, h0)
            ct = None
        # torch.transpose传入0，1表示将ht的0，1维度交换，而ms的transpose支持多维度的同时转置，需要传入perm，与tf类似
        ht = self.transpose(ht, (1, 0 ,2))[x_unsort_idx]  
        ht = self.transpose(ht, (1, 0 ,2))
        if self.only_use_last_hidden_state:
            return ht
        else:
            out = out[x_unsort_idx]
            if self.rnn_type =='LSTM':
                ct = self.transpose(ct, (1, 0 ,2))[x_unsort_idx]
                ct = self.transpose(ct, (1, 0 ,2))

            return out, (ht, ct)
