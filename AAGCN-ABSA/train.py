# -*- coding: utf-8 -*-
import argparse
import math
import numpy
import os

import mindspore
import mindspore.nn as nn
import mindspore.ops as ops
from mindspore import context, Model, Tensor, save_checkpoint , set_seed
from mindspore.common import initializer
from mindspore.communication import init
from mindspore.nn.transformer import CrossEntropyLoss
from mindspore.train.callback import Callback, CheckpointConfig, ModelCheckpoint
from sklearn import metrics

from data_utils import build_dataset
from models.aagcn import AAGCN


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_name', default='aagcn', type=str)
    parser.add_argument('--dataset', default='15_rest', type=str, help='14, 15_rest, 16_rest, 15_lap, 16_lap')
    parser.add_argument('--knowledge_base', default='senticnet', type=str, help='conceptnet, senticnet')
    parser.add_argument('--optimizer', default='adam', type=str)
    parser.add_argument('--initializer', default='xavier_uniform_', type=str)
    parser.add_argument('--learning_rate', default=0.001, type=float)
    parser.add_argument('--l2reg', default=0.00001, type=float)
    parser.add_argument('--num_epoch', default=100, type=int)
    parser.add_argument('--batch_size', default=16, type=int)
    parser.add_argument('--log_step', default=5, type=int)
    parser.add_argument('--embed_dim', default=300, type=int)
    parser.add_argument('--hidden_dim', default=300, type=int)
    parser.add_argument('--polarities_dim', default=3, type=int)
    parser.add_argument('--save', default=True, type=bool)
    parser.add_argument('--seed', default=1000, type=int)
    parser.add_argument('--device', default=None, type=str)
    parser.add_argument('--valset_ratio', default=0.1, type=float,
                        help='set ratio between 0 and 1 for validation support')
    parser.add_argument('--patience', default=5, type=int)

    parser.add_argument('--dataset_prefix', default='15_rest', type=str)

    parser.add_argument('--save_ckpt', default=True, type=bool)
    parser.add_argument('--save_ckpt_path', default='output', type=str)
    parser.add_argument('--keep_ckpt_max', default=5, type=int)


    return parser.parse_args()

def print_args(model, opt):
    n_trainable_params, n_nontrainable_params = 0, 0
    for p in model.get_parameters():
        n_params = ops.ReduceProd(keep_dims=False)(Tensor(p.shape))
        if p.requires_grad:
            n_trainable_params += n_params
        else:
            n_nontrainable_params += n_params
    print('n_trainable_params: {0}, n_nontrainable_params: {1}'.format(n_trainable_params, n_nontrainable_params))
    print('> training arguments:')
    for arg in vars(opt):
        print('>>> {0}: {1}'.format(arg, getattr(opt, arg)))

class EvalEngine():
    def __init__(self, model, test_dataset):
        self.model = model
        self.test_dataset = test_dataset

    def eval(self):
        self.model.set_train(False)
        n_test_correct, n_test_total = 0, 0
        t_targets_all, t_outputs_all = None, None
        for t_sample_batched in self.test_dataset:
            t_targets = t_sample_batched[1]
            t_outputs = self.model(t_sample_batched)

            n_test_correct += int((ops.Argmax(-1)(t_outputs) == t_targets).sum())
            n_test_total += len(t_outputs)

            if t_targets_all is None:
                t_targets_all = t_targets
                t_outputs_all = t_outputs
            else:
                t_targets_all = ops.Concat(axis=0)((t_targets_all, t_targets))
                t_outputs_all = ops.Concat(axis=0)((t_outputs_all, t_outputs))

        test_acc = n_test_correct / n_test_total
        t_targets_all = t_targets_all.asnumpy()
        t_outputs_all = ops.Argmax(-1)(t_outputs_all)
        t_outputs_all = t_outputs_all.asnumpy()
        f1 = metrics.f1_score(t_targets_all, t_outputs_all, labels=[0, 1, 2], average='macro')

        self.model.set_train(True)
        return test_acc, f1

class EvalCallBack(Callback):
    def __init__(self, net, model, opt, test_dataset):
        self.model = model
        self.opt = opt
        self.cur_epoch = 0
        self.cur_step = 0
        self.test_dataset = test_dataset
        self.eval_engine = EvalEngine(net, test_dataset)
        self.global_f1 = 0
        self.max_val_acc = 0
        self.max_val_f1 = 0
        self.eval_dataset = test_dataset

    def epoch_begin(self, run_context):
        print('======== cur epoch:', self.cur_epoch)

    def epoch_end(self, run_context):
        # 训练结束后进行测试精度
        if self.cur_epoch==self.opt.num_epoch:
            acc, f1 = self.eval_engine.eval()
            print('>> acc: {:.4f}, f1: {:.4f}'.format(acc, f1))
            log_path = 'log/{}_{}_{}_eval.txt'.format(self.opt.model_name, self.opt.dataset, self.opt.knowledge_base)
            with open(log_path, 'a', encoding='utf-8') as f:
                f.write('seed : {0:5d}, max_acc_avg: {1}, max_f1_avg: {2} \n'.format(opt.seed, acc, f1))
        self.cur_epoch+=1

    def step_begin(self, run_context):
        print('======== cur step:', self.cur_step)

    def step_end(self, run_context):
        cb_params = run_context.original_args()
        # 每隔 opt.log_step 个 step进行验证，保存当前最有模型
        if self.cur_step!=0 and self.cur_step % self.opt.log_step == 0:
            val_acc, val_f1 = self.eval_engine.eval()
            if val_acc > self.max_val_acc:
                self.max_val_acc = val_acc
            if val_f1 > self.max_val_f1:
                self.max_val_f1 = val_f1
                if self.opt.save and val_f1 > self.global_f1:
                    self.global_f1 = val_f1
                    path = 'state_dict/best_{}_{}_{}_eval.ckpt'.format(self.opt.model_name, self.opt.dataset, self.opt.knowledge_base)
                    save_checkpoint(cb_params.train_network, path)
                    print('>>> best model saved: ', path)
            print('>> acc: {:.4f}, f1: {:.4f}'.format(val_acc, val_f1))
            print('>> acc: {:.4f}, f1: {:.4f}'.format(self.max_val_acc, self.max_val_f1))
        self.cur_step+=1


def init_gparameters(net):
    for p in net.get_parameters():
        if p.requires_grad:
            if len(p.shape) > 1:
                p.set_data(initializer.initializer(initializer.XavierUniform(), p.shape, mindspore.float32))
            else:
                stdv = 1. / math.sqrt(p.shape[0])
                p.set_data(initializer.initializer(initializer.Uniform(scale=stdv), p.shape, mindspore.float32))

class NetWithLoss(nn.Cell):
    def __init__(self, backbone, loss_fn):
        super(NetWithLoss, self).__init__(auto_prefix=False)
        self._backbone = backbone
        self._loss_fn = loss_fn

    def construct(self, *input):
        polarity = Tensor(input[1], mindspore.int32)
        out = self._backbone(input)
        input_mask = Tensor(numpy.ones(polarity.shape).astype(numpy.float32))
        return self._loss_fn(out, polarity, input_mask)

if __name__ == '__main__':
    opt = parse_args()
    # 设置mindspore和numpy的随机种子
    set_seed(opt.seed)
    numpy.random.seed(opt.seed)

    # 下沉npu必需配置，表示执行的device_id和通信组
    device_id = int(os.getenv('DEVICE_ID', '0'))
    rank_size = int(os.getenv('RANK_SIZE', '1'))
    rank_id = int(os.getenv('RANK_ID', '0'))

    '''
    设置运行环境的context，参数可参考
    https://www.mindspore.cn/docs/zh-CN/master/api_python/mindspore/mindspore.set_context.html?highlight=set_context
    '''
    context.set_context(
        # mode=context.GRAPH_MODE, 
        mode=context.PYNATIVE_MODE, 
        # device_target='Ascend',
        device_target='CPU', 
        device_id=device_id
    )

    # 多卡训练通讯建立
    if rank_size>1:
        context.set_auto_parallel_context(device_num=rank_size, parallel_mode=context.ParallelMode.DATA_PARALLEL, gradient_mean=True)
        init()

    # 创建数据集
    train_dataset, test_dataset, embedding_matrix = build_dataset(
        dataset_prefix=opt.dataset_prefix, 
        knowledge_base=opt.knowledge_base,
    )
    # assert 0 <= opt.valset_ratio < 1
    # if opt.valset_ratio > 0:

    #     valset_len = int(len(train_dataset) * opt.valset_ratio)
    #     train_dataset, eval_dataset = random_split(train_dataset, (len(train_dataset) - valset_len, valset_len))
    # else:
    #     eval_dataset = test_dataset

    step_size = train_dataset.get_dataset_size()

    # 初始化网络
    net = AAGCN(embedding_matrix, opt)
    init_gparameters(net)
    print_args(net, opt)

    '''
    TODO:
    已验证mindspore.nn.SoftmaxCrossEntropyWithLogits(sparse=True,reduction="mean")等价torch.nn.CrossEntropyLoss()
    已验证mindspore.nn.transformer.CrossEntropyLoss
    待对比mindspore.nn.SoftmaxCrossEntropyWithLogits、mindspore.nn.transformer.CrossEntropyLoss
    '''
    loss = CrossEntropyLoss()
    # 连接 net 和 loss
    net_with_loss = NetWithLoss(net, loss)
    opt_adm = nn.Adam(net.trainable_params(), opt.learning_rate, weight_decay=opt.l2reg)
    # 连接 net、loss 和 opt_adm
    train_net = nn.TrainOneStepCell(net_with_loss, opt_adm)
    # set_train(True) 时会反向更行网络权重，
    train_net.set_train(True)
    
    # callback内实现step开始、结束以及epoch开始、结束时执行的操作
    callback = [
        EvalCallBack(net, train_net, opt, test_dataset)
    ]
    if opt.save_ckpt:
        config = CheckpointConfig(save_checkpoint_steps=step_size, keep_checkpoint_max=opt.keep_ckpt_max)
        ckpt_callback = ModelCheckpoint(
            prefix='{}_{}_{}'.format(opt.model_name, opt.dataset, opt.knowledge_base),
            directory=opt.save_ckpt_path, config=config, 
        )
        callback+=[ckpt_callback]
    model = Model(train_net)
    model.train(opt.num_epoch, train_dataset, callbacks=callback, sink_size=step_size, dataset_sink_mode=False)