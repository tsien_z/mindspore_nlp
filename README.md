# mindspore_nlp

#### 介绍
  本仓库保存使用开源mindspore框架迁移pcl 开源nlp网络范例。

#### 迁移步骤
1. mindspore与pytorch的主要区别

|pytorch|mindspore|
|--|--|
|子网继承torch.nn.Module|子网继承mindspore.nn.Cell|
|正向重写方法名为forward|正向重写方法名为construct|
|反向传播通过loss.backward()|TrainOneStepCell类完成|
|参数更新通过optimizer.step()|TrainOneStepCell类完成

2. api映射对应表

点击链接查询 https://www.mindspore.cn/docs/zh-CN/master/note/api_mapping/pytorch_api_mapping.html


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
